$(document).ready(function(){
  var average_val;
  var colorsArray=["#be1e2d","#e1ad41", "#9db84d","#00a14b","#e5618d"];
  var imagesArray2=["./icons/6.png","./icons/7.png", "./icons/8.png","./icons/9.png","./icons/10.png"];
  var textArray=["Неделава беше ужасна..", "Можеше и подобро.", "Уште една обична работна недела","Забавно беше", "WOW, одлична недела."];
  var webStorageData=['enjoyment_val', 'team_val', 'productivity_val', 'learning_val', 'stress_val' ];


  Init();
  function setMood(val){
    var mood_level=val;
    $('#imgSmiley').attr('src', imagesArray2[mood_level]);

    $('#average').html(average_val);
    $('#average').css('color',colorsArray[mood_level]);
    $('#mood-text').html(textArray[mood_level]);
    $('#mood-text').css('color',colorsArray[mood_level]);
    
    $('#team_img').attr('src', imagesArray2[mood_level]);
    $('#team_score').css('color',colorsArray[mood_level]);
  }

  function getLevel(val){
    if (val>=0 && val<10){
    return 0;   
    }
    else if(val>=10 && val<40){
      return 1;
    }else if(val>=40 && val<=60){;
      return 2;
    }else if(val>60 && val<=90){
      return 3;
    }else if (val>90 && val<=100){
      return 4;
    }
  }

  function getElData(el){
    var data_el_val=localStorage.getItem(el);
    var data_el_mood_level = getLevel(data_el_val);
        
    var name = el.slice(0, el.indexOf("_"));

    $('#'+name+'_score').html((data_el_val/10).toFixed(1));
    $('#'+name+'_img').attr('src', imagesArray2[data_el_mood_level]);
    $('#'+name+'_score').css('color',colorsArray[data_el_mood_level]);
    console.log(name);
    console.log('PRESMETAV '+name+' '+data_el_val);
  }

  function averageData(){
    var s=0;
    for(i=0;i<webStorageData.length;i++){
      s+=parseInt(localStorage.getItem(webStorageData[i]));
    }
    var average_val = (s/5).toFixed(1);
    var average_mood_level = getLevel(average_val);
    $('#average').html((average_val/10).toFixed(1));
    setMood(average_mood_level);  
    console.log('PRESMETAV SREDNO '+average_val);
  }
  function Init(){
    for(i=0;i<webStorageData.length;i++)
      getElData(webStorageData[i]);
    averageData();
  }
  
 $("#myBtn3").click(function(){
      $("#myModal3").modal({backdrop: "static"});
  });

});